<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'depublication_description' => 'Ce plugin permet de définir une date de dépublication
		pour les articles de SPIP (ainsi que le nouveau statut désiré
		au moment de la dépublication)',
	'depublication_slogan' => 'Ce plugin permet de définir une date de dépublication',
);
