2017/04/07: Version 2.3.2
-------------------------

```
FIXED:    Le texte de date de dépublication est bien enlevé en mode édition pour faire place au formulaire uniquement.
FIXED:    La date de dépublication s'affiche de préférence après la date de modification manuelle (si ce plugin est présent).
```

2016/12/12: Version 2.3.1
-------------------------

```
FIXED:    Appliquer réellement le statut demandé lors de la dépublication.
```

2016/04/29: Version 2.3.0
-------------------------

```
ADDED:    Formulaire de configuration
ADDED:    Permettre que les boucles affichent par défaut les éléments dépublies (configuration)
ADDED:    Critere `{depublies}` et `{!depublies}`
```


2014/07/24: Version 2.0.0
-------------------------

```
ADDED:    Ajout d'un statut 'depublie' pour les objets pouvant recevoir une date de dépublication
CHANGED:  Le statut par défaut après dépublication devient 'depublie'
```

2014/07/23: Version 2.0.0-beta
------------------------------

```
CHANGED:  Insertion des champs de date de depublication et statut en passant par le formulaire dater de SPIP 3 (et nettoyages)
BC-BREAK: La constante DEPUBLIER_DEFAUT_STATUT est maintenant nommée DEPUBLICATION_STATUT_PAR_DEFAUT
```


2014/07/23: Version 2.0.0-alpha
-------------------------------

```
ADDED:     Portage en SPIP 3
BC-BREAK:  Nécessite SPIP 3. Utiliser la version 1.* pour SPIP 2.1 (branche non maintenue).  
```


?: Version 1.0.0
----------------

```
ADDED:     Gestion de dépublication d'article en ajoutant un formulaire et 2 champs dans la table spip_articles
```
